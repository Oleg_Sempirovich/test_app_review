package com.example.myapplication.ui


import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.annotation.StringRes
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.filters.SmallTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.myapplication.R
import com.example.myapplication.presentation.activity.MainActivity
import com.example.myapplication.presentation.adapter.BaseDataBindAdapter
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@SmallTest
@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest1 {

    @get:Rule
    var activityScenarioRule = activityScenarioRule<MainActivity>()

    @Test
    fun testEvent() {
        val scenario = activityScenarioRule.scenario
        scenario.moveToState(Lifecycle.State.CREATED)
    }

    @Test
    fun startTest() {
        getButtonSurvey().perform(click())
        questionFragmentTest()
    }

    fun questionFragmentTest() {

        val actionNext = getButtonNextActionBar()
            .perform(click())

        val actionPrevious = getButtonPreviousActionBar()

        actionNext.perform(click())
        actionNext.perform(click())
        actionPrevious.perform(click())

        onView(allOf(withId(R.id.questionListRecyclerView), isDisplayed())).perform(
            RecyclerViewActions.actionOnItemAtPosition<BaseDataBindAdapter.BindingHolder>(
                0,
                setTextViewWithId(
                    R.id.editTextTextPersonName,
                    "red"
                )
            )
        )

        getButtonSubmit().perform(click())

        val viewSnackBar = onSnackBarButton(R.string.retry)
        if (viewSnackBar.isDisplayed())
            viewSnackBar.perform(click())
        else {
            actionNext.perform(click())
            onView(allOf(withId(R.id.questionListRecyclerView), isDisplayed())).perform(
                RecyclerViewActions.actionOnItemAtPosition<BaseDataBindAdapter.BindingHolder>(
                    0,
                    setTextViewWithId(
                        R.id.editTextTextPersonName,
                        "red"
                    )
                )
            )
            getButtonSubmit().perform(click())
            val viewSnackBar1 = onSnackBarButton(R.string.retry)
            if (viewSnackBar1.isDisplayed())
                viewSnackBar1.perform(click())
        }
        getButtonBack().perform(click())
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }

    private fun getButtonSurvey() = onView(
        allOf(
            withId(R.id.buttonStartSurvey), withText("Start Survey"),
            childAtPosition(
                childAtPosition(
                    withId(R.id.main_nav_host_fragment),
                    0
                ),
                0
            ),
            isDisplayed()
        )
    )
    private fun getButtonPreviousActionBar() =
        onView(
            allOf(
                withId(R.id.action_previous), withText("Previous"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.action_bar),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )

    private fun getButtonNextActionBar() =
        onView(
            allOf(
                withId(R.id.action_next), withText("Next"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.action_bar),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )

    private fun onSnackBarButton(@StringRes withText: Int): ViewInteraction {
        return onView(
            allOf(
                withId(com.google.android.material.R.id.snackbar_action),
                withText(withText)
            )
        )
    }

    private fun getButtonSubmit() =
        onView(
            allOf(
                withId(R.id.button), withText("Submit"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.questionListRecyclerView),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )

    private fun getButtonBack() = onView(
        allOf(
            withContentDescription("Navigate up"),
            childAtPosition(
                allOf(
                    withId(R.id.action_bar),
                    childAtPosition(
                        withId(R.id.action_bar_container),
                        0
                    )
                ),
                1
            ),
            isDisplayed()
        )
    )

}

fun ViewInteraction.isDisplayed(): Boolean {
    return try {
        check(matches(ViewMatchers.isDisplayed()))
        true
    } catch (e: NoMatchingViewException) {
        false
    }
}

fun setTextViewWithId(id: Int, text: String): ViewAction? {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View>? {
            return null
        }

        override fun getDescription(): String {
            return "Click on a child view with specified id."
        }

        override fun perform(uiController: UiController?, view: View) {
            val v = view.findViewById<EditText>(id)
            v.setText(text)
        }
    }
}

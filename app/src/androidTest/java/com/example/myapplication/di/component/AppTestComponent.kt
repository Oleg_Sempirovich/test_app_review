package com.example.myapplication.di.component

import com.example.myapplication.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Init by Dagger for change substitution logic
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class
    ]
)
interface AppTestComponent {
    @Component.Builder
    interface Builder {
        fun build(): AppTestComponent
        @BindsInstance
        fun appModule(appContext: App): Builder
    }
}
package com.example.myapplication.repositories

import com.example.myapplication.data.datasource.QuestionCachedDataSourceImpl
import com.example.myapplication.data.datasource.QuestionRemoteDataSourceImpl
import com.example.myapplication.data.repository.QuestionRepositoryImpl
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.model.network.DatAnswer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AnswerRepositoriesImplTest {

     val questionRemoteDataSourceImpl: QuestionRemoteDataSourceImpl =  mock(QuestionRemoteDataSourceImpl::class.java)

     val questionCachedDataSourceImpl: QuestionCachedDataSourceImpl =  mock(QuestionCachedDataSourceImpl::class.java)

     val questionRepositoryImpl: QuestionRepositoryImpl =  QuestionRepositoryImpl(questionRemoteDataSourceImpl,questionCachedDataSourceImpl)

    @Test
    fun testAnswerSuccess() {
        runBlockingTest {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRemoteDataSourceImpl.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Success(mock(ResponseBody::class.java)))
            assert(questionRepositoryImpl.sendDataAnswer(dataMock) is ResultWrapper.Success)
        }
    }

    @Test
    fun testAnswerHttpError() {
        runBlocking {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRemoteDataSourceImpl.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Error.HttpError(400))
            assert(questionRepositoryImpl.sendDataAnswer(dataMock)  is ResultWrapper.Error.HttpError)
        }
    }

    @Test
    fun testAnswerGeneralError() {
        runBlocking {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRemoteDataSourceImpl.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Error.GeneralError(Throwable()))
            assert(questionRepositoryImpl.sendDataAnswer(dataMock)  is ResultWrapper.Error.GeneralError)
        }
    }

}
package com.example.myapplication.repositories

import com.example.myapplication.data.datasource.QuestionCachedDataSourceImpl
import com.example.myapplication.data.datasource.QuestionRemoteDataSourceImpl
import com.example.myapplication.data.repository.QuestionRepositoryImpl
import com.example.myapplication.domain.handleerror.ResultWrapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class QuestionRepositoriesImplTest {

     val questionRemoteDataSourceImpl: QuestionRemoteDataSourceImpl =  mock(QuestionRemoteDataSourceImpl::class.java)

     val questionCachedDataSourceImpl: QuestionCachedDataSourceImpl =  mock(QuestionCachedDataSourceImpl::class.java)

     val questionRepositoryImpl: QuestionRepositoryImpl =  QuestionRepositoryImpl(questionRemoteDataSourceImpl,questionCachedDataSourceImpl)

    @Test
    fun testGetQuestionListSuccess() {
        runBlockingTest {
            Mockito.`when`(questionRemoteDataSourceImpl.getQuestionsList())
                .thenReturn(ResultWrapper.Success(arrayListOf()))
            assert(questionRepositoryImpl.getQuestionsList() is ResultWrapper.Success)
        }
    }

    @Test
    fun testGetQuestionListHttpError() {
        runBlockingTest {
            Mockito.`when`(questionRemoteDataSourceImpl.getQuestionsList()).thenReturn(
            ResultWrapper.Error.HttpError(400))
            assert(questionRepositoryImpl.getQuestionsList() is  ResultWrapper.Error.HttpError)
        }
    }

    @Test
    fun testGetQuestionListGeneralError() {
        runBlockingTest {
            Mockito.`when`(questionRemoteDataSourceImpl.getQuestionsList()).thenReturn(
                ResultWrapper.Error.GeneralError(Throwable()))
            assert(questionRepositoryImpl.getQuestionsList() is ResultWrapper.Error.GeneralError)
        }
    }

}
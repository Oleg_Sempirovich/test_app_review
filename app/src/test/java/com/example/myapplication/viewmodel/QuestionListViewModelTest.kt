package com.example.myapplication.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.model.network.DatAnswer
import com.example.myapplication.domain.model.network.DataQuestion
import com.example.myapplication.domain.repository.question.QuestionRepository
import com.example.myapplication.domain.usecase.DataAnswerUseCase
import com.example.myapplication.domain.usecase.DataQuestionUseCase
import com.example.myapplication.presentation.state.StateError
import com.example.myapplication.presentation.viewmodel.QuestionListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class QuestionListViewModelTest {

    lateinit var questionRepository: QuestionRepository

    lateinit var questionUseCase: DataQuestionUseCase

    lateinit var answerUseCase: DataAnswerUseCase

    lateinit var questionViewModel: QuestionListViewModel


    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun beforeTest() {
        questionRepository = mock(QuestionRepository::class.java)
        questionUseCase = DataQuestionUseCase(questionRepository)
        answerUseCase = DataAnswerUseCase(questionRepository)
    }


    @Test
    fun testQuestionsListSuccess() {
        runBlockingTest {
            Mockito.`when`(questionRepository.getQuestionsList())
                .thenReturn(ResultWrapper.Success(arrayListOf()))
            questionViewModel = QuestionListViewModel(questionUseCase, answerUseCase)
            assertEquals(questionViewModel.dataQuestionLiveData.value, arrayListOf<DataQuestion>())
        }
    }

    @Test
    fun testQuestionsListHttpError() {
        runBlockingTest {
            Mockito.`when`(questionRepository.getQuestionsList()).thenReturn(
                ResultWrapper.Error.HttpError(400)
            )
            questionViewModel = QuestionListViewModel(questionUseCase, answerUseCase)
            assert(questionViewModel.errorLiveData.value is StateError.ErrorGeneral)
        }
    }

    @Test
    fun testQuestionsListGeneralError() {
        runBlockingTest {
            Mockito.`when`(questionRepository.getQuestionsList()).thenReturn(
                ResultWrapper.Error.GeneralError(Throwable())
            )
            questionViewModel = QuestionListViewModel(questionUseCase, answerUseCase)
            assert(questionViewModel.errorLiveData.value is StateError.ErrorGeneral)
        }
    }


    @Test
    fun testSendAnswerSuccess() {
        runBlockingTest {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRepository.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Success(mock(ResponseBody::class.java)))
            questionViewModel = QuestionListViewModel(questionUseCase, answerUseCase)
            questionViewModel.clickSubmitQuestion(dataMock)
            assert(questionViewModel.answerLiveDataSent.value is DatAnswer)
        }
    }

    @Test
    fun testSendAnswerHttpError() {
        runBlockingTest {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRepository.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Error.HttpError(400))
            questionViewModel = QuestionListViewModel(questionUseCase, answerUseCase)
            questionViewModel.clickSubmitQuestion(dataMock)
            assert(questionViewModel.errorLiveData.value is StateError.ErrorRetry)
        }
    }

    @Test
    fun testSendAnswerGeneralError() {
        runBlockingTest {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRepository.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Error.GeneralError(Throwable()))
            questionViewModel = QuestionListViewModel(questionUseCase, answerUseCase)
            questionViewModel.clickSubmitQuestion(dataMock)
            assert(questionViewModel.errorLiveData.value is StateError.ErrorGeneral)
        }
    }
}



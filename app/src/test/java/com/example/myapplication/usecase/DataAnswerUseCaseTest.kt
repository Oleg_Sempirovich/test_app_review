package com.example.myapplication.usecase

import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.model.network.DatAnswer
import com.example.myapplication.domain.repository.question.QuestionRepository
import com.example.myapplication.domain.usecase.DataAnswerUseCase
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class DataAnswerUseCaseTest {

    lateinit var questionRepository: QuestionRepository

    lateinit var answerUseCase: DataAnswerUseCase

    @Before
    fun beforeTest() {
        questionRepository = mock(QuestionRepository::class.java)
        answerUseCase = DataAnswerUseCase(questionRepository)
    }

    @Test
    fun testAnswerSuccess() {
        runBlocking {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRepository.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Success(mock(ResponseBody::class.java)))
            assert(answerUseCase.execute(dataMock) is ResultWrapper.Success)
        }
    }

    @Test
    fun testAnswerHttpError() {
        runBlocking {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRepository.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Error.HttpError(400))
            assert(answerUseCase.execute(dataMock) is ResultWrapper.Error.HttpError)
        }
    }

    @Test
    fun testAnswerGeneralError() {
        runBlocking {
            val dataMock = mock(DatAnswer::class.java)
            Mockito.`when`(questionRepository.sendDataAnswer(dataMock))
                .thenReturn(ResultWrapper.Error.GeneralError(Throwable()))
            assert(answerUseCase.execute(dataMock) is ResultWrapper.Error.GeneralError)
        }
    }
}
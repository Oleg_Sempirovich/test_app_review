package com.example.myapplication.usecase

import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.repository.question.QuestionRepository
import com.example.myapplication.domain.usecase.DataQuestionUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DataQuestionUseCaseTest {

     lateinit var questionRepository: QuestionRepository

     lateinit var questionUseCase: DataQuestionUseCase

    @Before
    fun beforeTest() {
        questionRepository = mock(QuestionRepository::class.java)
        questionUseCase = DataQuestionUseCase(questionRepository)
    }


    @Test
    fun testGetQuestionListSuccess() {
        runBlockingTest {
            Mockito.`when`(questionRepository.getQuestionsList())
                .thenReturn(ResultWrapper.Success(arrayListOf()))
            assert(questionUseCase.execute() is ResultWrapper.Success)
        }
    }

    @Test
    fun testGetQuestionListHttpError() {
        runBlockingTest {
            Mockito.`when`(questionRepository.getQuestionsList()).thenReturn(
                ResultWrapper.Error.HttpError(400))
            assert(questionUseCase.execute() is ResultWrapper.Error.HttpError)
        }
    }

    @Test
    fun testGetQuestionListGeneralError() {
        runBlockingTest {
            Mockito.`when`(questionRepository.getQuestionsList()).thenReturn(
                ResultWrapper.Error.GeneralError(Throwable()))
            assert(questionUseCase.execute() is ResultWrapper.Error.GeneralError)
        }
    }
}
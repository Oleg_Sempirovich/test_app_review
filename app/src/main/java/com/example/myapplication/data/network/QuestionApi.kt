package com.example.myapplication.data.network

import com.example.myapplication.domain.model.network.DatAnswer
import com.example.myapplication.domain.model.network.DataQuestion
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface QuestionApi {

    @GET("/questions")
    suspend fun getQuestionListData(): List<DataQuestion>

    @POST("/question/submit")
    suspend fun sendDataAnswer(@Body datAnswer: DatAnswer):ResponseBody
}
package com.example.myapplication.data.dao.questiondata

import androidx.room.*
import com.example.myapplication.domain.entity.DataQuestionEntity

@Dao
interface DataQuestionDao{

    @Query("SELECT * FROM data_question")
    suspend fun getQuestionEntityAll(): List<DataQuestionEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(entities: List<DataQuestionEntity>)

    @Delete
    fun delete(question: DataQuestionEntity)
}


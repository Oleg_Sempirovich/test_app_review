package com.example.myapplication.data.repository

import com.example.myapplication.data.datasource.QuestionCachedDataSourceImpl
import com.example.myapplication.data.datasource.QuestionRemoteDataSourceImpl
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.model.network.DatAnswer
import com.example.myapplication.domain.repository.question.QuestionRepository
import okhttp3.Response
import okhttp3.ResponseBody
import javax.inject.Inject

class QuestionRepositoryImpl @Inject constructor(
    private val remote: QuestionRemoteDataSourceImpl,
    private val cache: QuestionCachedDataSourceImpl,
) : QuestionRepository {

    override suspend fun getQuestionsList() =
        remote.getQuestionsList()

    override suspend fun sendDataAnswer(dataAnswer: DatAnswer): ResultWrapper<ResponseBody> =
        remote.sendDataAnswer(dataAnswer)

    override suspend fun getQuestionListEntity() =
        cache.getDataQuestionsEntityList()

}

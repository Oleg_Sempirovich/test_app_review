package com.example.myapplication.data.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.myapplication.data.dao.questiondata.DataQuestionDao
import com.example.myapplication.domain.entity.DataQuestionEntity

@Database(
    entities = [
        DataQuestionEntity::class],
    version = 1
)

abstract class AppDB : RoomDatabase() {

    companion object {
        const val NAME = "app_database"
    }

    abstract fun userDao(): DataQuestionDao

}

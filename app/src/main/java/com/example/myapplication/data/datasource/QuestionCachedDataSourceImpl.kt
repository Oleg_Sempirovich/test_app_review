package com.example.myapplication.data.datasource

import com.example.myapplication.data.dao.questiondata.DataQuestionDao
import com.example.myapplication.domain.datasource.QuestionCachedDataSource
import com.example.myapplication.domain.entity.DataQuestionEntity
import javax.inject.Inject

open class QuestionCachedDataSourceImpl @Inject constructor(
    private val userDao: DataQuestionDao
) : QuestionCachedDataSource {

    override suspend fun insertQuestionsList(questionList: List<DataQuestionEntity>) {
        userDao.insertAll(questionList)
    }

    override suspend fun getDataQuestionsEntityList() =
        userDao.getQuestionEntityAll()
}

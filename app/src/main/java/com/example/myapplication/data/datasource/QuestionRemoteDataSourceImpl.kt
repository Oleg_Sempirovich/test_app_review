package com.example.myapplication.data.datasource

import com.example.myapplication.data.network.QuestionApi
import com.example.myapplication.domain.datasource.QuestionRemoteDataSource
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.handleerror.safeApiCall
import com.example.myapplication.domain.model.network.DatAnswer
import com.example.myapplication.domain.model.network.DataQuestion
import okhttp3.ResponseBody
import javax.inject.Inject

open class QuestionRemoteDataSourceImpl @Inject constructor(
    private val api: QuestionApi,
) : QuestionRemoteDataSource{

    override suspend fun getQuestionsList(): ResultWrapper<List<DataQuestion>> {
        return safeApiCall { api.getQuestionListData() }
    }

    override suspend fun sendDataAnswer(datAnswer: DatAnswer): ResultWrapper<ResponseBody> {
        return safeApiCall { api.sendDataAnswer(datAnswer) }
    }
}

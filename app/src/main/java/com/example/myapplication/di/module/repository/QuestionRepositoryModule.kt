package com.example.myapplication.di.module.repository

import com.example.myapplication.data.repository.QuestionRepositoryImpl
import com.example.myapplication.di.module.datasource.QuestionDataSourceModule
import com.example.myapplication.domain.repository.question.QuestionRepository
import dagger.Binds
import dagger.Module

@Module(includes = [QuestionDataSourceModule::class])
abstract class QuestionRepositoryModule {

    @Binds
    abstract fun bindUserRepository(
        userRepository: QuestionRepositoryImpl
    ): QuestionRepository

}


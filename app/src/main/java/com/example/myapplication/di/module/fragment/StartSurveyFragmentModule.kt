package com.example.myapplication.di.module.fragment

import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModel
import com.example.myapplication.R
import com.example.myapplication.di.module.repository.QuestionRepositoryModule
import com.example.myapplication.di.scope.viewmodel.ViewModelKey
import com.example.myapplication.domain.model.network.DataQuestion
import com.example.myapplication.presentation.adapter.BaseDataBindAdapter
import com.example.myapplication.presentation.viewmodel.QuestionListViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class StartSurveyFragmentModule
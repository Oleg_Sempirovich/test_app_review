package com.example.myapplication.di.module.activity

import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.di.module.fragment.QuestionsListFragmentModule
import com.example.myapplication.di.module.fragment.StartSurveyFragmentModule
import com.example.myapplication.di.provider.factory.ViewModelFactory
import com.example.myapplication.di.scope.PerFragment
import com.example.myapplication.presentation.fragment.QuestionListFragment
import com.example.myapplication.presentation.fragment.StartSurveyFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @Binds
    internal abstract fun bindViewModelFactory(
        factory: ViewModelFactory
    ): ViewModelProvider.Factory

    @PerFragment
    @ContributesAndroidInjector(modules = [StartSurveyFragmentModule::class])
    internal abstract fun provideStartSurveyFragment(): StartSurveyFragment


    @PerFragment
    @ContributesAndroidInjector(modules = [QuestionsListFragmentModule::class])
    internal abstract fun provideUserListFragment(): QuestionListFragment


}

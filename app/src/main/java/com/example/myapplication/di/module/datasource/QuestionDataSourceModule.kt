package com.example.myapplication.di.module.datasource

import com.example.myapplication.data.dao.AppDB
import com.example.myapplication.data.datasource.QuestionCachedDataSourceImpl
import com.example.myapplication.data.datasource.QuestionRemoteDataSourceImpl
import com.example.myapplication.data.network.QuestionApi
import com.example.myapplication.di.provider.RetrofitProvider
import com.example.myapplication.domain.datasource.QuestionCachedDataSource
import com.example.myapplication.domain.datasource.QuestionRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class QuestionDataSourceModule {

    @Binds
    abstract fun bindUserRemoteDataSource(
        remote: QuestionRemoteDataSourceImpl
    ): QuestionRemoteDataSource

    @Binds
    abstract fun bindUserCachedDataSource(
        cached: QuestionCachedDataSourceImpl
    ): QuestionCachedDataSource

    companion object {
        @Provides
        fun provideUserApi(retrofitServiceProvider: RetrofitProvider) =
            retrofitServiceProvider.createService(QuestionApi::class.java)

        @Provides
        fun provideUserDb(appDB: AppDB) =
            appDB.userDao()
    }

}

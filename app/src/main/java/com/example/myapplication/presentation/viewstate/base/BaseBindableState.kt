package com.example.myapplication.presentation.viewstate.base

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import com.example.myapplication.delegate.BindableStateDelegate


open class BaseBindableState : BindingObservable {

    private val callbacks: PropertyChangeRegistry = PropertyChangeRegistry()

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    override fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    override fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }


    fun <R : BaseBindableState, T : Any?> bindable(value: T, bindingRes: Int): BindableStateDelegate<R, T> =
        BindableStateDelegate(value, bindingRes)


}

package com.example.myapplication.presentation.fragment

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentStartSurveyBinding
import com.example.myapplication.presentation.fragment.base.BaseFragment


class StartSurveyFragment :
    BaseFragment<FragmentStartSurveyBinding>(R.layout.fragment_start_survey) {

    private val navController by lazy { findNavController() }

    override fun initBinding() {
        (activity as AppCompatActivity).supportActionBar?.hide()
        initButtonView()
    }

    private fun initButtonView() {
        binding.buttonStartSurvey.setOnClickListener {
            navController.navigate(StartSurveyFragmentDirections.actionStartSurveyFragmentToUserlistfragment())
        }
    }
}
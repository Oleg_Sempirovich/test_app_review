package com.example.myapplication.presentation.fragment

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentQuestionListBinding
import com.example.myapplication.extension.hideKeyboard
import com.example.myapplication.extension.initDataAdapter
import com.example.myapplication.extension.showSnackBarError
import com.example.myapplication.presentation.adapter.BaseDataBindAdapter
import com.example.myapplication.presentation.fragment.base.BaseFragment
import com.example.myapplication.presentation.state.StateError
import com.example.myapplication.presentation.viewmodel.QuestionListViewModel
import com.example.myapplication.presentation.viewstate.QuestionViewStateItem
import javax.inject.Inject


class QuestionListFragment :
    BaseFragment<FragmentQuestionListBinding>(R.layout.fragment_question_list) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val vm by viewModels<QuestionListViewModel> { viewModelFactory }

    private lateinit var questionAdapter: BaseDataBindAdapter<QuestionViewStateItem>

    private lateinit var optionMenu: Menu

    private  var actionBar: ActionBar? = null

    private val navController by lazy { findNavController() }

    override fun initBinding() {
        initActionBar()
        initRecyclerView()
        initSubmittedTitleQuestions()
    }

    private fun initActionBar() {
        setHasOptionsMenu(true)
        actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.apply {
          title = ""
          show()
          setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun initSubmittedTitleQuestions() {
        binding.submittedQuestions.text = getString(R.string.question_submitted, 0)
    }

    private fun initRecyclerView() {
        questionAdapter = binding.questionListRecyclerView.initDataAdapter(vm)
        binding.questionListRecyclerView.changePosition = {
            actionBar?.title =
                getString(R.string.question_page, it + 1, questionAdapter.dataList.size)
        }
    }

    override fun observeData() {
        vm.answerLiveDataSent.observe(this) { datAnswer->
            questionAdapter.dataList.find {it.id == datAnswer.id}?.isEnableEditText = false
            binding.submittedQuestions.text =
                getString(R.string.question_submitted, questionAdapter.dataList.count { !it.isEnableEditText })
            showSnackBarError(getString(R.string.success))
        }

        vm.errorLiveData.observe(this) { stateError ->
           when(stateError){
               is StateError.ErrorRetry ->{
                   showSnackBarError(getString(R.string.failure),
                       action = View.OnClickListener { vm.clickSubmitQuestion(stateError.value) })
               }
               is StateError.ErrorGeneral->{
                   showSnackBarError(stateError.value)
               }
           }

        }

        vm.loadingLockScreenLiveData.observe(this) {
            if (it) binding.loadingContainer.visibility = View.VISIBLE
            else binding.loadingContainer.visibility = View.GONE
            hideKeyboard()
        }

        vm.dataQuestionLiveData.observe(this) { listDataQuestion ->
            optionMenu.findItem(R.id.action_next).isEnabled = true
            questionAdapter.dataList = listDataQuestion.map {
               QuestionViewStateItem(
                    it.id,
                    it.question
                )
            }
            actionBar?.title = getString(R.string.question_page, 1, questionAdapter.dataList.size)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        optionMenu = menu
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_list_questions, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_next -> {
                optionMenu.findItem(R.id.action_previous).isEnabled = true
                item.isEnabled = binding.questionListRecyclerView.next()
            }
            R.id.action_previous -> {
                optionMenu.findItem(R.id.action_next).isEnabled = true
                item.isEnabled = binding.questionListRecyclerView.preview()
            }
            android.R.id.home ->{
                navController.popBackStack()
            }
        }
        return true
    }

}
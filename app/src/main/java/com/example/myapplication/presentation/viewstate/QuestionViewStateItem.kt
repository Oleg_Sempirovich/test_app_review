package com.example.myapplication.presentation.viewstate

import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.example.myapplication.domain.model.network.DatAnswer
import com.example.myapplication.presentation.viewstate.base.BaseBindableState

open class QuestionViewStateItem(
    val id: Int,
    question: String,
    answer: String? = null,
    isEnableEditText: Boolean = true
) : BaseBindableState() {


    @get:Bindable
    var questionText by bindable(question, BR.questionText)

    @get:Bindable
    var answerText  by bindable(answer, BR.answerText)

    @get:Bindable
    var isEnableEditText by bindable(isEnableEditText, BR.enableEditText)

    @Bindable("answerText", "enableEditText")
    fun  isEnableButton()  = isEnableEditText && !answerText.isNullOrEmpty()

    fun convertToDataQuestion() = DatAnswer(id,answerText?:"")

}
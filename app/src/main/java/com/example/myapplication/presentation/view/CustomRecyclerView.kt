package com.example.myapplication.presentation.view

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class CustomRecyclerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    var isNeedTouchScroll = false

    var changePosition:((position:Int)->Unit)? = null

    init {
        initScrollControl()
        initScrollTouchScroll()
    }

    private fun initScrollControl() {
        addOnScrollListener(object : OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == SCROLL_STATE_IDLE) {
                    isNeedTouchScroll = false
                    changePosition?.invoke(getCurrentItem())
                }
            }
        })
    }

    private fun initScrollTouchScroll() {
        layoutManager = object : LinearLayoutManager(context, HORIZONTAL, false) {
            override fun canScrollHorizontally(): Boolean {
                return isNeedTouchScroll
            }

        }
    }

    private fun getCurrentItem() =
        (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

    fun preview(): Boolean {
        val position = getCurrentItem()
        isNeedTouchScroll = true
        smoothScrollToPosition(position - 1)
        return position > 1
    }

    fun next(): Boolean {
        val adapter: Adapter<*> = adapter ?: return false
        val position = getCurrentItem()
        val count = adapter.itemCount
        isNeedTouchScroll = true
        smoothScrollToPosition(position + 1)
        return (position < count - 2)
    }

}
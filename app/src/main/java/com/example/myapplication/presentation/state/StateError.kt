package com.example.myapplication.presentation.state

import com.example.myapplication.domain.model.network.DatAnswer

sealed class StateError {
        data class ErrorRetry(val value: DatAnswer) : StateError()
        data class ErrorGeneral(val value: String) : StateError()
    }
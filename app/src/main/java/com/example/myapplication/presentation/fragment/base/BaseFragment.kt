package com.example.myapplication.presentation.fragment.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import dagger.android.support.DaggerFragment

abstract class BaseFragment<VB : ViewDataBinding>(
    @LayoutRes private val contentLayoutId: Int
) : DaggerFragment(){
    protected lateinit var binding: VB

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, contentLayoutId, container, false)
        initBinding()
        observeData()
        return binding.root
    }

    open fun initBinding() = Unit

    open fun observeData() = Unit

}
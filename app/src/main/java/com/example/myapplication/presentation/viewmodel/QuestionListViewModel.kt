package com.example.myapplication.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.model.network.DatAnswer
import com.example.myapplication.domain.model.network.DataQuestion
import com.example.myapplication.domain.usecase.DataAnswerUseCase
import com.example.myapplication.domain.usecase.DataQuestionUseCase
import com.example.myapplication.presentation.base.SingleLiveEvent
import com.example.myapplication.presentation.state.StateError
import kotlinx.coroutines.launch
import javax.inject.Inject


class QuestionListViewModel  @Inject constructor(private val questionUseCase: DataQuestionUseCase,
                                                 private val dataAnswerUseCase: DataAnswerUseCase
) : ViewModel() {

    private val _loadingLockScreenLiveData = MutableLiveData<Boolean>()
    val loadingLockScreenLiveData: LiveData<Boolean> = _loadingLockScreenLiveData

    private val _errorLiveData = MutableLiveData<StateError>()
    val errorLiveData: LiveData<StateError> = _errorLiveData

    private val _dataQuestionLiveData = SingleLiveEvent<List<DataQuestion>>()
    val dataQuestionLiveData: LiveData<List<DataQuestion>> = _dataQuestionLiveData

    private val _answerLiveDataSent = SingleLiveEvent<DatAnswer>()
    val answerLiveDataSent: LiveData<DatAnswer> = _answerLiveDataSent


    init {
        initUserData()
    }

    private fun initUserData(){
        _loadingLockScreenLiveData.value = true
        viewModelScope.launch {

            when (val result = questionUseCase.execute()) {
                is ResultWrapper.Success -> {
                    _dataQuestionLiveData.value =  result.value
                }
                is ResultWrapper.Error.HttpError -> {
                    _errorLiveData.value = StateError.ErrorGeneral("Error get data")
                }
                else -> {
                    _errorLiveData.value = StateError.ErrorGeneral("Another error")
            }
            }
            _loadingLockScreenLiveData.value = false
        }
    }

    private fun sendDataAnswer(dataAnswer: DatAnswer){
        _loadingLockScreenLiveData.value = true
        viewModelScope.launch {
            when (val result = dataAnswerUseCase.execute(dataAnswer)) {
                is ResultWrapper.Success -> {
                    _answerLiveDataSent.value = dataAnswer
                }
                is ResultWrapper.Error.HttpError -> {
                   if(result.codeData == 400)
                    _errorLiveData.value = StateError.ErrorRetry(dataAnswer)
                }
                else -> {
                    _errorLiveData.value = StateError.ErrorGeneral("Another error")
                }
            }
            _loadingLockScreenLiveData.value = false
        }

    }

    fun clickSubmitQuestion(dataAnswer: DatAnswer){
               sendDataAnswer(dataAnswer)
    }
}

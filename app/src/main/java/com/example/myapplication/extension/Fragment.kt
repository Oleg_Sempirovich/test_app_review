package com.example.myapplication.extension

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import com.google.android.material.snackbar.Snackbar


fun Fragment.hideKeyboard() {
    val imm: InputMethodManager =
        activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view?.windowToken, 0)
}

    fun Fragment.showSnackBarError(text:String, @ColorInt color:Int= Color.RED, action: View.OnClickListener? = null){
        val snackbar = Snackbar.make(view!!, text, Snackbar.LENGTH_LONG)
        action?.also {
            snackbar.setAction(R.string.retry,action)
            snackbar.setActionTextColor(color)
        }
        snackbar.show()
}

package com.example.myapplication.extension

import androidx.annotation.ColorInt
import androidx.appcompat.widget.Toolbar
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.presentation.adapter.BaseDataBindAdapter

fun Toolbar.setState(
    textData: String, @ColorInt colorBackground: Int
) {
    subtitle = null
    title = textData
    setBackgroundColor(colorBackground)
}

fun <T> RecyclerView.initDataAdapter(
    vm: ViewModel
): BaseDataBindAdapter<T> {
    return  BaseDataBindAdapter<T>(
        com.example.myapplication.R.layout.item_question,
        BR.questionViewStateItem,
        arrayListOf(),
        vm
    ).also {
        adapter = it
    }

}
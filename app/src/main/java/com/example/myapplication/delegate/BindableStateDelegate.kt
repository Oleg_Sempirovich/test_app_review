package com.example.myapplication.delegate

import com.example.myapplication.presentation.viewstate.base.BindingObservable
import kotlin.reflect.KProperty

class BindableStateDelegate<in R : BindingObservable, T : Any?>(
    private var value: T,
    private val bindingEntry: Int
) {

    operator fun getValue(thisRef: R, property: KProperty<*>): T = value

    operator fun setValue(thisRef: R, property: KProperty<*>, value: T) {
        this.value = value
        thisRef.notifyPropertyChanged(bindingEntry)
    }
}

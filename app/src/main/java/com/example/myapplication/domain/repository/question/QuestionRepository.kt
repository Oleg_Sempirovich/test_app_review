package com.example.myapplication.domain.repository.question

import com.example.myapplication.domain.model.network.DataQuestion
import com.example.myapplication.domain.entity.DataQuestionEntity
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.model.network.DatAnswer
import okhttp3.Response
import okhttp3.ResponseBody

interface QuestionRepository {

    suspend fun getQuestionsList(): ResultWrapper<List<DataQuestion>>

    suspend fun sendDataAnswer(dataAnswer: DatAnswer): ResultWrapper<ResponseBody>

    suspend fun getQuestionListEntity(): List<DataQuestionEntity>
}

package com.example.myapplication.domain.usecase

import com.example.myapplication.domain.repository.question.QuestionRepository
import javax.inject.Inject

class DataQuestionUseCase @Inject constructor(
    private val repository: QuestionRepository
) {
    suspend fun execute() = repository.getQuestionsList()
}

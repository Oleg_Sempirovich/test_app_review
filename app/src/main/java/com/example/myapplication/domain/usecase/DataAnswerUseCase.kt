package com.example.myapplication.domain.usecase

import com.example.myapplication.domain.model.network.DatAnswer
import com.example.myapplication.domain.repository.question.QuestionRepository
import javax.inject.Inject

open class DataAnswerUseCase @Inject constructor(
    private val repository: QuestionRepository
) {
    suspend fun execute(dataAnswer: DatAnswer) = repository.sendDataAnswer(dataAnswer)
}

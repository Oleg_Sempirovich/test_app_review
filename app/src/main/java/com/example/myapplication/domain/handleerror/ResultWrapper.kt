package com.example.myapplication.domain.handleerror

import retrofit2.HttpException


sealed class ResultWrapper<out T> {
    data class Success<out T>(val value: T) : ResultWrapper<T>()
    sealed class Error : ResultWrapper<Nothing>() {
        data class HttpError(val codeData: Int? = null, val errorData: Any? = null) : Error()
        data class GeneralError(val throwable: Throwable) : Error()
    }
}

suspend fun <T> safeApiCall(apiCall: suspend () -> T): ResultWrapper<T> {
    return try {
        ResultWrapper.Success(apiCall.invoke())
    } catch (throwable: Throwable) {
        when (throwable) {
            is HttpException -> {
                ResultWrapper.Error.HttpError(throwable.code(), throwable.message())
            }
            else -> {
                ResultWrapper.Error.GeneralError(throwable)
            }
        }
    }
}
package com.example.myapplication.domain.model.network

import com.squareup.moshi.Json

open class DatAnswer(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "answer") val answer: String
)
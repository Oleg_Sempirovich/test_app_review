package com.example.myapplication.domain.model.network

import com.squareup.moshi.Json
import nl.qbusict.cupboard.annotation.Ignore

data class DataQuestion(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "question") val question: String
)
package com.example.myapplication.domain.datasource

import com.example.myapplication.domain.model.network.DataQuestion
import com.example.myapplication.domain.entity.DataQuestionEntity
import com.example.myapplication.domain.handleerror.ResultWrapper
import com.example.myapplication.domain.model.network.DatAnswer
import okhttp3.Response
import okhttp3.ResponseBody

interface QuestionRemoteDataSource {

    suspend fun getQuestionsList(): ResultWrapper<List<DataQuestion>>

    suspend fun sendDataAnswer(datAnswer: DatAnswer): ResultWrapper<ResponseBody>
}

interface QuestionCachedDataSource {

    suspend fun insertQuestionsList(
        questionList: List<DataQuestionEntity>
    )
    suspend fun getDataQuestionsEntityList():
       List<DataQuestionEntity>

}
